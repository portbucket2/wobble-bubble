﻿using Unity.Entities;
using Unity.Mathematics;

public struct VertexAgentID : ISharedComponentData
{
    public int agentID;
}
//public struct VertexIndex : IComponentData
//{
//    public int index;
//}
public struct VertexData : IComponentData
{
    public int index;
    public float3 position;
    public float3 normal;
}
//public struct VertexNormal : IComponentData
//{
//    public float3 initial;
//    public float3 current;
//}
//public struct VertexDeform : IComponentData
//{
//    public float deformHeight;
//}
