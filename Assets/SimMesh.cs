﻿using UnityEngine;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Entities;
using Unity.Collections;
using System.Collections.Generic;
using System.Linq;
public class SimMesh : MonoBehaviour
{

    bool selfLog;
    public static List<SimMesh> simMeshList = new List<SimMesh>();
    public EntityManager entityManager;
    
    public Mesh mesh;
    MeshRenderer mrend;
    int agentID;
    int VC;

    public AgentController selfAgent;


    public void AddItemToRepresent(AgentController agentCon)
    {
        if (deformingAgents.Contains(agentCon)) return;

        deformingAgents.Add(agentCon);
    }
    public void RemoveItemToRepresent(AgentController agentCon)
    {
        if (!deformingAgents.Contains(agentCon)) return;

        deformingAgents.Remove(agentCon);
    }

    void Awake()
    {
        selfAgent = this.GetComponentInParent<AgentController>();
        selfAgent.simMesh = this;
        selfAgent.onSizeChanged += OnSizeChanged;

        this.selfLog = selfAgent.selfLog;

        simMeshList.Add(this);
        agentID = simMeshList.Count - 1;
        //Debug.Log("VES S");
        Application.targetFrameRate = 3000;

        entityManager = World.Active.EntityManager;
        MeshFilter mfilter = this.GetComponent<MeshFilter>();
        mrend = this.GetComponent<MeshRenderer>();
        mesh = mfilter.mesh;
        mesh = Instantiate(mesh);
        mfilter.mesh = mesh;
        Vector3[] vposI = mesh.vertices;
        Vector3[] normI = mesh.normals;
        Vector2[] uvI = mesh.uv;
        VC = vposI.Length;
        Init();
        GenerateWorkVertEntities(vposI,normI,uvI);
    }

    public float size;
    void OnSizeChanged(float size)
    {
        this.size = size;
        this.transform.localScale = new Vector3(size,size,size);

#if UNITY_EDITOR
        selfAgent.placeholder.transform.localScale= size*2*Vector3.one;
#endif
    }



    private void GenerateWorkVertEntities(Vector3[] vposI,Vector3[] normI,Vector2[] uvI)
    {
        EntityArchetype vertexArchetype = entityManager.CreateArchetype(
           typeof(VertexAgentID),
           typeof(VertexData)
           );
        NativeArray<Entity> entities = new NativeArray<Entity>(VC, Allocator.Persistent);
        for (int i = 0; i < VC; i++)
        {
           // Debug.Log(VC);
            entities[i] = entityManager.CreateEntity(vertexArchetype);
            entityManager.SetSharedComponentData(entities[i], new VertexAgentID
            {
                agentID = agentID
            });
            entityManager.SetComponentData(entities[i], new VertexData
            {
                index = i,
                position = vposI[i],
                normal = normI[i]
            });

        }
        entities.Dispose();
    }

    public NativeArray<Vector3> vposNativeArray;    //copy job output
    public NativeArray<Vector3> normNativeArray;    //copy job output
    public NativeArray<Color> vcolNativeArray;      //copy job output


    public List<AgentController> deformingAgents;
    public List<AgentController> orderedDeformingAgents;
    public NativeArray<float3> deformerPoints;
    public NativeArray<float> deformerStrengths;
    public NativeArray<float> deformerHeightContributionsForJob;
    public NativeArray<bool> tnl_active;

    private void Init()
    {
        deformerHeightContributionsForJob = new NativeArray<float>(100,Allocator.Persistent);
        vposNativeArray = new NativeArray<Vector3>(VC, Allocator.Persistent);
        normNativeArray = new NativeArray<Vector3>(VC, Allocator.Persistent);
        vcolNativeArray = new NativeArray<Color>(VC, Allocator.Persistent);

        deformingAgents = new List<AgentController>();
        orderedDeformingAgents = new List<AgentController>();
        deformerPoints = new NativeArray<float3>(100, Allocator.Persistent);
        deformerStrengths = new NativeArray<float>(100, Allocator.Persistent);
        tnl_active = new NativeArray<bool>(100, Allocator.Persistent);

        selfAgent.SetRepresentative(selfAgent);
    }
    private void OnDestroy()
    {
        selfAgent.onSizeChanged -= OnSizeChanged;

        deformerHeightContributionsForJob.Dispose();
        vposNativeArray.Dispose();
        normNativeArray.Dispose();
        vcolNativeArray.Dispose();

        deformerPoints.Dispose();
        deformerStrengths.Dispose();
        tnl_active.Dispose();
    }

    public JobHandle VertModJob;
    int frameCount;

    private void LateUpdate()
    {
        if (disableRender) return;

        VertModJob.Complete();
        int frameOffset = frameCount++ % 3;
        mesh.SetVertices(vposNativeArray);
        mesh.SetNormals(normNativeArray);
        //switch (frameOffset)
        //{
        //    case 0:
        //        break;
        //    case 1:
        //        mesh.SetNormals(normNativeArray);
        //        break;
        //    case 2:
        //        //mesh.SetColors(vcolNativeArray);
        //        break;
        //}
    }

    public float distanceFrom(Transform trans)
    {
        return (this.transform.position - trans.position).magnitude;
    }


    public int tnlCount = 0;

    bool _disableRender;
    bool disableRender 
    {
        get { return _disableRender; }
        set
        {
            _disableRender = value;
            mrend.enabled = !value;
#if UNITY_EDITOR
            selfAgent.placeholderRenderer.enabled = value;
#endif
        }
    }
    public bool UpdateNativeDeformerData()
    {
        disableRender = ControllerPlayer.instance.IsTooFarToRender(this.transform);
        if (disableRender)
        {
            return false;
        }
        orderedDeformingAgents.Clear();

        while (deformingAgents.Count>0)
        {
            float closestDist = float.MaxValue;
            AgentController ac = null;
            for (int i = 0; i < deformingAgents.Count; i++)
            {
                float dist = deformingAgents[i].simMesh.distanceFrom(this.transform);
                if (ac == null || dist < closestDist)
                {
                    closestDist = dist;
                    ac = deformingAgents[i];
                }
            }
            orderedDeformingAgents.Add(ac);
            deformingAgents.Remove(ac);
        }
        deformingAgents.AddRange(orderedDeformingAgents);


        for (int i = 0; i < deformingAgents.Count; i++)
        {
            Vector3 worldPoint = deformingAgents[i].transform.position;// + new Vector3(1, 0, 0);
            //if(selfLog)Debug.Log(worldPoint);
            deformerPoints[i] = this.transform.InverseTransformPoint(worldPoint);
            deformerStrengths[i] = deformingAgents[i].threatRadius;

            tnlCount = 0;
            if (i > 0)
            {
                tnl_active[i - 1] = false;
                float diff= (deformingAgents[0].transform.position - deformingAgents[i].transform.position).magnitude - deformerStrengths[0]-deformerStrengths[i];
                
                if (diff < (deformerStrengths[0] + deformerStrengths[1]) / 8)
                {
                    tnlCount++;
                    tnl_active[i - 1] = true;
                }

            }
        }
        //if (selfLog) Debug.Log(tnlCount);
        return true;
    }
}
