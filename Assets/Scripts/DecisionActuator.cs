﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionActuator : MonoBehaviour
{
    public AgentController agentCommander;


    public bool Move(Vector3 direction)
    {
        direction.y = 0;
        direction.Normalize();

        Vector3 newPos = this.transform.position + (agentCommander.movementSpeed * Time.deltaTime) * direction;

        bool moveIsOutOfBounds= false;

        if (newPos.x > BoundsManager.xPos)
        {
            newPos.x = BoundsManager.xPos;
            moveIsOutOfBounds = true;
        }
        if (newPos.x < BoundsManager.xNeg)
        {
            newPos.x = BoundsManager.xNeg;
            moveIsOutOfBounds = true;
        }
        if (newPos.z > BoundsManager.zPos)
        {
            newPos.z = BoundsManager.zPos;
            moveIsOutOfBounds = true;
        }
        if (newPos.z < BoundsManager.zNeg)
        {
            newPos.z = BoundsManager.zNeg;
            moveIsOutOfBounds = true;
        }



        this.transform.position = newPos;
        return moveIsOutOfBounds;
    }
}
