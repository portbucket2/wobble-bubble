﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerPlayer : AgentController
{

    public static ControllerPlayer instance;
    public float disableRendereDistance = 5;
    public JoyStick myJoyStick;
    public bool IsTooFarToRender(Transform tr)
    {
        return Vector3.Distance(tr.position,this.transform.position)>disableRendereDistance;
    }
    protected override void OnAwake()
    {
        base.OnAwake();
        instance = this;
        myJoyStick = this.GetComponent<JoyStick>();
        myJoyStick.onDirectionRecieved += OnDirectionRecieved;
    }
    
    void OnDirectionRecieved(Vector2 vec)
    {
        Vector3 commandVec = new Vector3(vec.x,0,vec.y);
        actuator.Move(commandVec);
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetMouseButton(0)&&CamController.instance)
    //    {
    //        Camera cam = CamController.instance.camRef;

    //        Ray rayTouch = cam.ScreenPointToRay(Input.mousePosition);
    //        Ray rayPlayer = new Ray(agentTransform.position+new Vector3(0,10,0),Vector3.down) ;

    //        Debug.DrawRay(rayTouch.origin, rayTouch.direction, Color.red);
    //        Debug.DrawRay(rayPlayer.origin, rayPlayer.direction, Color.green);

    //        RaycastHit rchTouch;
    //        RaycastHit rchPlayer;
    //        if (Physics.Raycast(rayTouch, out rchTouch, 10) && Physics.Raycast(rayPlayer, out rchPlayer, 10))
    //        {
    //            Vector3 dirVec = (rchTouch.point - rchPlayer.point).normalized;
    //            actuator.Move(dirVec);
    //        }
    //        else
    //        {
    //            throw new System.Exception("Bad Area!");
    //        }


    //    }
    //}
}
