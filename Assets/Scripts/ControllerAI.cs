﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerAI : AgentController
{
    public const float DISTANCE_OF_SCARE = 5;

    //public float maxTurnLerpRate = 10;

    protected override void OnAwake()
    {
        base.OnAwake();
    }



    void Update()
    {
        if (a_fear)
        {
            float fearDistance = (a_fear.transform.position - actuator.transform.position).magnitude - a_fear.threatRadius + this.threatRadius;
            if (fearDistance < DISTANCE_OF_SCARE)
            {
                DoFearMove();
            }
            else if(a_food)
            {
                DoFoodMove();
            }
        }
        else if (a_food)
        {
            DoFoodMove();
        }
    }

    void DoFearMove()
    {
        actuator.Move(this.transform.position - a_fear.transform.position);
    }
    void DoFoodMove()
    {
        actuator.Move(a_food.transform.position - this.transform.position);
    }
}
