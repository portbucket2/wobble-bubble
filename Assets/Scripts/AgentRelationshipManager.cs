﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentRelationshipManager : MonoBehaviour
{
    public static AgentRelationshipManager instance;

    public List<AgentController> activeAgents;
    public List<AgentController> passiveAgents;

    private void Awake()
    {
        AgentController.onAgentCreated += OnAgentCreated;
        AgentController.onAgentKilled += OnAgentKilled;
    }
    private void OnDestroy()
    {
        AgentController.onAgentCreated -= OnAgentCreated;
        AgentController.onAgentKilled -= OnAgentKilled;
    }
    public void OnAgentCreated(AgentController agent)
    {
        if (agent.isAgentActive)
        {
            activeAgents.Add(agent);
        }
        else
        {
            passiveAgents.Add(agent);
        }
    }
    public void OnAgentKilled(AgentController agent)
    {
        if (agent.isAgentActive)
        {
            activeAgents.Remove(agent);
        }
        else
        {
            passiveAgents.Remove(agent);
        }
    }
    private void Update()
    {
        ResolveRelations();
        ResolveRepresentationResponsibilities();
    }

    public void ResolveRelations()
    {
        for (int i = 0; i < activeAgents.Count; i++)
        {
            AgentController agent1 = activeAgents[i];

            for (int j = 0; j < activeAgents.Count; j++)
            {
                AgentController agent2 = activeAgents[j];

                if (agent1.threatRadius > agent2.threatRadius)
                {
                    AssessAsNewFood(agent: agent1, food_new: agent2);
                    AssessAsNewFear(agent: agent2, fear_new: agent1);
                }
                else if (agent1.threatRadius < agent2.threatRadius)
                {
                    AssessAsNewFood(agent: agent2, food_new: agent1);
                    AssessAsNewFear(agent: agent1, fear_new: agent2);
                }
                else
                {
                    //they are equal do nothing
                }

            }

            for (int j = 0; j < passiveAgents.Count; j++)
            {
                AgentController agent2 = passiveAgents[j];
                AssessAsNewFood(agent: agent1, food_new: agent2);
            }
        }

        for (int i = 0; i < passiveAgents.Count; i++)
        {
            AgentController food_agent = passiveAgents[i];

            for (int j = 0; j < activeAgents.Count; j++)
            {
                AgentController threat_agent = activeAgents[j];

                AssessAsNewFear(agent: food_agent, fear_new: threat_agent);
            }
        }
    }

    public void AssessAsNewFood(AgentController agent, AgentController food_new)
    {
        AgentController food_old = agent.a_food;
        if (!food_old) 
        { 
            agent.a_food = food_new;
            return;
        }
        float oldDistance = (food_old.transform.position - agent.transform.position).magnitude + food_old.threatRadius - agent.threatRadius; 
        float oldInterest = food_old.foodValue / (oldDistance);

        float newDistance = (food_new.transform.position - agent.transform.position).magnitude + food_new.threatRadius - agent.threatRadius;
        float newInterest = food_new.foodValue / (newDistance);

        if (newInterest > oldInterest)
        {
            agent.a_food = food_new;
        }
    }
    public void AssessAsNewFear(AgentController agent, AgentController fear_new)
    {
        AgentController fear_old = agent.a_fear; 
        if (!fear_old)
        {
            agent.a_fear = fear_new;
            return;
        }
        float oldDistance = (fear_old.transform.position - agent.transform.position).magnitude - fear_old.threatRadius + agent.threatRadius;

        float newDistance = (fear_new.transform.position - agent.transform.position).magnitude - fear_new.threatRadius + agent.threatRadius;

        if (newDistance < oldDistance)
        {
            agent.a_fear = fear_new;
        }
    }

    public void ResolveRepresentationResponsibilities()
    {
        for (int i = 0; i < activeAgents.Count; i++)
        {
            AgentController agent = activeAgents[i];
            agent.SetRepresentative(agent);
            if (agent.a_fear)
            {
                float fear_distance = (agent.a_fear.transform.position - agent.transform.position).magnitude + agent.threatRadius;

                if (fear_distance <= agent.a_fear.threatRadius * 3)
                {
                    agent.SetRepresentative(agent.a_fear);
                }
            }

        }
        for (int i = 0; i < passiveAgents.Count; i++)
        {
            AgentController agent = passiveAgents[i];
            agent.SetRepresentative(agent);
            if (agent.a_fear)
            {
                float fear_distance = (agent.a_fear.transform.position - agent.transform.position).magnitude + agent.threatRadius;

                if (fear_distance <= agent.a_fear.threatRadius * 3)
                {
                    agent.SetRepresentative(agent.a_fear);
                }
            }

        }
    }

    //public void ChooseRepresentative(AgentController agent, AgentController representative_new)
    //{
    //    AgentController representative_old = agent.representative;
    //    if (!agent.representative) 
    //    {
    //        agent.representative = representative_new;
    //        return;
    //    }

    //    float distance_old =


    //}
}
