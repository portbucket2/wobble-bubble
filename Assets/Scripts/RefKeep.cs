﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefKeep : MonoBehaviour
{
    public bool isTestMode;

    public static RefKeep instance;

    public GameObject blobPrefab;

    public static float time { get { return Time.time; } }
    void Awake()
    {
        instance = this;
        Application.targetFrameRate = 3000;
    }

}
