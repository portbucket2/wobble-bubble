﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentController : MonoBehaviour
{
    public bool selfLog;
    [Header("Settings")]
    [SerializeField] public bool isAgentActive;
    [SerializeField] public float threatRadius = 1;
    [SerializeField] public float foodValue = 1;
    [SerializeField] public float movementSpeed = 3;


    [Space]
    [Header("Others")]
    public GameObject placeholder;
    internal MeshRenderer placeholderRenderer;



    [HideInInspector] public DecisionActuator actuator;
    [HideInInspector] public Transform agentTransform;

    public static Action<AgentController> onAgentCreated;
    public static Action<AgentController> onAgentKilled;

    public Action<float> onSizeChanged;
    public Action<AgentController> onRepresentativeChanged;

    public SimMesh simMesh;

    public AgentController a_food;
    public AgentController a_fear;

    private AgentController representative;

    public void SetRepresentative(AgentController agentCon)
    {
        if(representative) representative.simMesh.RemoveItemToRepresent(this);
        representative = agentCon;
        representative.simMesh.AddItemToRepresent(this);
    }



    private void Awake()
    {
        OnAwake();

    }

    protected virtual void OnAwake()
    {

        placeholderRenderer = placeholder.GetComponent<MeshRenderer>();
#if !UNITY_EDITOR
        placeholderRenderer.enabled = false;
#endif
        agentTransform = Instantiate(RefKeep.instance.blobPrefab, this.transform).transform;
        agentTransform.localScale = Vector3.one;
        agentTransform.localPosition = Vector3.zero;
        agentTransform.localRotation = Quaternion.identity;

        actuator = this.gameObject.AddComponent<DecisionActuator>();

        actuator.agentCommander = this;
    }

    private void Start()
    {
        onSizeChanged?.Invoke(threatRadius);
        onAgentCreated?.Invoke(this);
    }

    protected virtual void OnKilled()
    {
        onAgentKilled?.Invoke(this);
    }
}
