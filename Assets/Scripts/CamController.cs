﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    public static CamController instance;
    public Camera camRef;
    public BoxCollider colliderRef;

    [Header("Cam Positioning Settings")]
    public Vector3 defaultOffset = new Vector3(0,5,0);
    public float lerpRate = 3;
    
    
    Vector3 targetPosition;


    private void Awake()
    {
        instance = this;
    }

    void Update()
    {
        if (ControllerPlayer.instance)
        {
            targetPosition = ControllerPlayer.instance.agentTransform.position + defaultOffset;
        }

        this.transform.position = Vector3.Lerp(this.transform.position,targetPosition, lerpRate*Time.deltaTime);
    }
}
