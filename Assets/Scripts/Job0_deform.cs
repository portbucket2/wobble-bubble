﻿using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;
using UnityEngine;

[BurstCompile]
public struct Job0_deform : IJobChunk
{

    [ReadOnly] public float lambda;
    [ReadOnly] public float omega_t;
    [ReadOnly] public float AMP;
    [ReadOnly] public float2 waveDir;
    [ReadOnly] public float3 worldPos;
    [ReadOnly] public float sM;
    [ReadOnly] public int deformerCount;
    [ReadOnly] public NativeArray<float> deformerRadius;
    [ReadOnly] public NativeArray<float3> deformerPosition;



    [ReadOnly] public NativeArray<bool> tnl_active;
    //[ReadOnly] public NativeArray<float> tnl_centreRads;
    //[ReadOnly] public NativeArray<float> tnl_outerRads;



    public NativeArray<Vector3> vertPos;
    public NativeArray<Vector3> vertNor;
    //public NativeArray<float> heightsPerDeformer;

    [ReadOnly] public ArchetypeChunkComponentType<VertexData> vposType;
    //public ArchetypeChunkComponentType<VertexNormal> vnorType;



    public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
    {
        NativeArray<VertexData> vposArray = chunk.GetNativeArray(vposType);
        float sM_Square = math.pow( sM,2.5f);

        float3 defaultPos = new float3(0, -10f, 0);
        float3 defaultNor = new float3(0, 1, 0);

        float3 deformerPos_0 = deformerPosition[0];
        float deformerRad_0 = deformerRadius[0];

        float waveDir_angleX = math.atan2(waveDir.y, waveDir.x);
        float sinWDAX = math.sin(waveDir_angleX);
        float cosWDAX = math.cos(waveDir_angleX);

        float3 deformerPosOfInterest;
        //float deformerRadOfInterest;
        for (int v = 0; v < vposArray.Length; v++)
        {
            VertexData vposComp = vposArray[v];
            float x = vposComp.position.x;
            float y = -10f;
            float z = vposComp.position.z;

            int affectorCount = 0;

            defaultPos.x = x;
            defaultPos.z = z;
            vposComp.normal = defaultNor;
            vposComp.position = defaultPos;
            for (int d = 0; d < deformerCount; d++)
            {
                deformerPosOfInterest = deformerPosition[d];
                float xd = (x - deformerPosOfInterest.x)*sM;
                float zd = (z - deformerPosOfInterest.z)*sM;

                float expanded_rad = deformerRadius[d] *1.0787f;
                float rt = expanded_rad * expanded_rad - xd * xd - zd * zd;
                if (rt > 0)
                {
                    float ygain = math.sqrt(rt);
                    float yoffset = (expanded_rad* 3 / 8);
                    float ynew = (ygain - yoffset) / sM;
                    vposComp = Affect(affectorCount, deformerPosOfInterest, vposComp, ynew);
                    affectorCount++;
                }
            }

            //affectorCount = 0;
            for (int i = 0; i < deformerCount-1; i++)
            {
                if (tnl_active[i])
                {
                    int t = i + 1;
                    deformerPosOfInterest = deformerPosition[t];
                    float2 O1 = new float2(deformerPos_0.x, deformerPos_0.z) * sM;
                    float2 O2 = new float2(deformerPosOfInterest.x, deformerPosOfInterest.z) * sM;
                    float2 P = new float2(x, z) * sM;
                    float dist_O1O2 = math.distance(O1, O2);
                    float dist_O1P = math.distance(P, O1);
                    float dist_O1Q = math.dot(P - O1, math.normalize(O2 - O1));
                    float dist_O2Q = math.dot(P - O2, math.normalize(O1 - O2));

                    float R1 = deformerRad_0;
                    float R2 = deformerRadius[t];

                    float frac1 = 1;// math.clamp( math.sqrt(R2/R1),0,1);
                    float frac2 = 0.9f;
                    float fracC = 0.9f;
                    float pwr1 = 4f;
                    float pwr2 = 4f;
                    float m = 1;


                    if(dist_O1O2>R1+R2)
                    {
                        float strechRatio = ((dist_O1O2-R1-R2) / ((R1+R2)/8));
                        frac1 = math.lerp(frac1, 1, strechRatio);
                        frac2 = math.lerp(frac2, 0.4f, strechRatio);
                        fracC = math.lerp(fracC, 1, strechRatio);
                    }
                    else if (dist_O1O2 > R1)
                    {
                        float sinkRatio = ((R1 + R2 - dist_O1O2) / R2);
                        frac1 = math.lerp(frac1, 1, sinkRatio);
                        frac2 = math.lerp(frac2, 1, sinkRatio);
                        fracC = math.lerp(fracC, 1, sinkRatio);
                        m = math.lerp(m, 1/1000, sinkRatio);

                    }
                    float _Re1 = R2 * frac1;
                    float _Re2 = R2 * frac2;

                    float _Rc = ((_Re1 < _Re2) ? _Re1 : _Re2) * fracC;

                    //float _Rd = _Re - _Rc;
                    float dist_O1E1 = math.sqrt(R1 * R1 - _Re1 * _Re1);
                    float dist_O2E2 = math.sqrt(R2 * R2 - _Re2 * _Re2);



                    //float2 Q = O1 + math.normalize(O2-O1)*dist_O1Q;

                    float dist_PQ = math.sqrt( dist_O1P*dist_O1P - dist_O1Q*dist_O1Q);

                    float dist_E1Q = dist_O1Q - dist_O1E1;
                    float dist_E2Q = dist_O2Q - dist_O2E2;
                    float dist_E1E2 = dist_O1O2 - dist_O1E1 - dist_O2E2;

                    //_Rx =m*x^pwr+_Rc
                    float x_e1 = math.pow((_Re1 - _Rc) / m, 1 / pwr1);
                    float x_e2 = math.pow((_Re2 - _Rc) / m, 1 / pwr2);
                    float mult = dist_E1E2 / (x_e1 + x_e2);
                    float dist_E1C = x_e1 *mult;

                    float sdist_CQ = dist_E1Q - dist_E1C;//if negative its closer to 0th deformer

                    float x_c = sdist_CQ / mult;
                    float lerpF = dist_E1Q / dist_E1E2;

                    if (dist_E1Q > 0 && dist_E2Q > 0)
                    {
                        float pwr;
                        if (x_c < 0)
                        {
                            pwr = pwr1;
                            x_c = -x_c;
                        }
                        else
                        {
                            pwr = pwr2;
                        }
                        float t_rad = (m*math.pow(x_c, pwr) + _Rc)* 1.0787f;
                        float yoffset = (t_rad * 3 / 8)/ sM;

                        if (dist_PQ <= t_rad)
                        {
                            float ynew = math.sqrt( (t_rad * t_rad - dist_PQ * dist_PQ) / sM_Square);
                            vposComp = Affect(affectorCount, math.lerp(new float3(O1.x,0,O1.y), new float3(O2.x, 0, O2.y), lerpF), vposComp, ynew- yoffset);
                            affectorCount++;
                        }
                    }


                }
            }



            float3 pos = vposComp.position;

            float wave_x = math.dot(new float2(pos.x, pos.z), waveDir);
            float theta = omega_t + (2 * math.PI * wave_x/lambda);
            float wave_y = AMP*math.sin(theta);


            bool isVisible = affectorCount > 0;

            if (isVisible)
            {
                float ang_p = math.atan(AMP * math.cos(theta) * 2 * math.PI / lambda);
                if (ang_p > math.PI / 2)
                {
                    ang_p -= math.PI;
                }
                float ang_n = ang_p + math.PI / 2;


                float3 wave_normal_dir;
                if (ang_n == math.PI / 2 || ang_n == math.PI * 3 / 2)
                {
                    wave_normal_dir = new float3(0, 1, 0);
                }
                else
                {

                    float wy = math.sin(ang_n);
                    float wxz = math.cos(ang_n);


                    wave_normal_dir.x = wxz * cosWDAX;
                    wave_normal_dir.y = wy;
                    wave_normal_dir.z = wxz * sinWDAX;
                }
                vposComp.normal = math.lerp(vposComp.normal, wave_normal_dir, 0.35f);
                vertNor[vposComp.index] = vposComp.normal;

            }




            //y=mx


            pos.y *= (1+wave_y);

            vposComp.position = pos;
            vertPos[vposComp.index] = vposComp.position;
        }
    }

    VertexData Affect(int previousAffectorCount, float3 normalRoot, VertexData vposComp, float ynew)
    {

        if (previousAffectorCount > 0 && ynew <= vposComp.position.y)
        {
            return vposComp;
        }
        else
        {
            vposComp.position = new float3(vposComp.position.x, ynew, vposComp.position.z);
            vposComp.normal = math.normalize(vposComp.position - normalRoot);// - new float3(0,  yoffset/sM_Square, 0))) ;
            return vposComp;
        }
    }

}

