﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BoundsManager : MonoBehaviour
{
    public static float xPos;
    public static float xNeg;
    public static float zPos;
    public static float zNeg;




    public Transform xposTrans;
    public Transform xnegTrans;
    public Transform zposTrans;
    public Transform znegTrans; 
    public Vector2 dimentions;
    public float offset = 0;
    private void Awake()
    {
        xPos = xposTrans.position.x - offset;
        xNeg = xnegTrans.position.x + offset;
        zPos = zposTrans.position.z - offset;
        zNeg = znegTrans.position.z + offset;
    }

    public void SetBounds()
    {
        xposTrans.position = this.transform.position + new Vector3(offset + dimentions.x / 2, 0, 0);
        xnegTrans.position = this.transform.position - new Vector3(offset + dimentions.x / 2, 0, 0);
        zposTrans.position = this.transform.position + new Vector3(0, 0, offset + dimentions.y / 2);
        znegTrans.position = this.transform.position - new Vector3(0, 0, offset + dimentions.y / 2);
    }

}
#if UNITY_EDITOR
[CustomEditor(typeof(BoundsManager))]
public class BoundsManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        BoundsManager bm = (BoundsManager)target;

        base.OnInspectorGUI();
        if (GUILayout.Button("Set Bounds", GUILayout.Height(40)))
        {
            bm.SetBounds();
        }
    }
}
#endif