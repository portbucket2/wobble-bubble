﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BGTileManager : MonoBehaviour
{

    public List<GameObject> gridItems;

    public Vector2 tileDimentions;
    public Vector2Int dim;

    public GameObject prefab;
    public void SetTiles()
    {
        for (int i = gridItems.Count -1; i >=0; i--)
        {
            GameObject go = gridItems[i];
            DestroyImmediate(go);
            gridItems.RemoveAt(i);
        }

        Vector3 startpoint = new Vector3( -(dim.x-1)*tileDimentions.x/2,0,-(dim.y-1)*tileDimentions.y/2);
        for (int i = 0; i < dim.x; i++)
        {
            for (int j = 0; j < dim.y; j++)
            {
                GameObject go = Instantiate(prefab,this.transform);
                go.name = string.Format("Tile {0}_{1}",i,j);
                gridItems.Add(go);

                go.transform.localPosition = startpoint + new Vector3(tileDimentions.x*i,0, tileDimentions.y*j);
            }
        }
    }

}
#if UNITY_EDITOR
[CustomEditor(typeof(BGTileManager))]
public class BGTileManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        BGTileManager bm = (BGTileManager)target;

        base.OnInspectorGUI();
        if (GUILayout.Button("Set Tiles", GUILayout.Height(40)))
        {
            bm.SetTiles();
        }
    }
}
#endif

