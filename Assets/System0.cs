﻿using UnityEngine;
using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;

[UpdateInGroup(typeof(SimulationSystemGroup))]
public class System0 : JobComponentSystem
{

    EntityQuery basicQuery;
    ArchetypeChunkComponentType<VertexData> vpos_rw;
    //ArchetypeChunkComponentType<VertexData> vpos_ro;




    protected override void OnCreate()
    {
        base.OnCreate();
        basicQuery = this.GetEntityQuery(ComponentType.ReadOnly<VertexData>(),typeof(VertexAgentID));
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        vpos_rw = this.GetArchetypeChunkComponentType<VertexData>(false);
        //vpos_ro = this.GetArchetypeChunkComponentType<VertexData>(true);
        
        JobHandle overallDependencies = inputDeps;

        for (int i = 0; i < SimMesh.simMeshList.Count; i++)
        {
            SimMesh simMesh = SimMesh.simMeshList[i];
            if (!simMesh.UpdateNativeDeformerData())
            {
                continue;
            }
            JobHandle iterationDependencies = inputDeps;
            basicQuery.SetSharedComponentFilter<VertexAgentID>(new VertexAgentID { agentID = i });
            Job0_deform dfJob = new Job0_deform
            {
                lambda = 1,
                omega_t = math.PI * RefKeep.time,
                AMP = 0.05f,
                worldPos = simMesh.transform.position,
                waveDir = new float2(1,1),
                sM = simMesh.size,
                deformerCount = simMesh.deformingAgents.Count,
                deformerRadius = simMesh.deformerStrengths,
                deformerPosition = simMesh.deformerPoints,
                tnl_active = simMesh.tnl_active,
                vertPos = simMesh.vposNativeArray,
                vertNor = simMesh.normNativeArray,

                vposType = vpos_rw
            };
            iterationDependencies = dfJob.Schedule(basicQuery, iterationDependencies);


            simMesh.VertModJob = iterationDependencies;
            overallDependencies = JobHandle.CombineDependencies(overallDependencies, iterationDependencies);

        }

        return overallDependencies;
    }
}


